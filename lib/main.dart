import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:get/route_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:s3mom/router.dart' as router;
import 'package:flutter/services.dart';
import 'package:s3mom/screens/scroll_view_contents.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

GetStorage storage;

void showAlertDialog(String title, List<Widget> widgets,
        {Function onConfirm, String textConfirm, Color titleBgColor}) =>
    Get.defaultDialog(
      barrierDismissible: true,
      backgroundColor: MainColors.pink,
      title: title,
      titleStyle: TextStyle(
          color: Colors.white,
          backgroundColor: titleBgColor ?? Colors.transparent),
      content: Container(
          height: Get.height * .5,
          width: Get.width,
          color: Colors.white,
          padding: EdgeInsets.all(8),
          child: SingleChildScrollView(child: Column(children: widgets))),
      textConfirm: textConfirm ?? "ย้อนกลับ",
      confirmTextColor: Colors.white,
      onConfirm: () => onConfirm != null ? onConfirm() : Get.back(),
    );

redTextSpan(text) =>
    "$text".textSpan.red500.fontFamily('prompt').size(14).bold.make();
blackTextSpan(text) =>
    "$text".textSpan.black.fontFamily('prompt').size(14).make();
boldTextSpan(text) =>
    "$text".textSpan.black.fontFamily('prompt').size(14).bold.make();
void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: MainColors.pink, // navigation bar color
    statusBarColor: MainColors.pink, // status bar color
  ));
  await GetStorage.init();
  storage = GetStorage();
  return runApp(MyApp());
}

showScrollView(BuildContext context, String title, List<String> paths,
    {int initPage, Map sounds}) async {
  Get.to(() => ScrollViewContents(
      initPage: initPage, paths: paths, title: title, sounds: sounds));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: '3s For New Mom',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        textTheme: Theme.of(context).textTheme.apply(
              bodyColor: MainColors.darkBlue,
              displayColor: MainColors.darkBlue,
              fontFamily: 'Prompt',
            ),
      ),
      initialRoute: router.homePage,
      onGenerateRoute: router.generateRoute,
      debugShowCheckedModeBanner: false,
    );
  }
}

class UndefinedView extends StatelessWidget {
  final String name;
  const UndefinedView({Key key, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Route for $name is not defined'),
      ),
    );
  }
}
