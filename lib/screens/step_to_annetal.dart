import 'package:flutter/material.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/screens/home_page.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:s3mom/widgets/page_template.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:get/get.dart';

class StepToAnnetal extends StatefulWidget {
  const StepToAnnetal({Key key}) : super(key: key);

  @override
  _StepToAnnetalState createState() => _StepToAnnetalState();
}

class _StepToAnnetalState extends State<StepToAnnetal> {
  int page = 1;
  PageController pageController = new PageController();
  String youtube1 = "-8Ei-Q05RX0", youtube2 = "swf_EsVTBFA";
  List<Map> questionList = [
    {"section": "ประวัติอดีต"},
    {
      "check": false,
      "question": "1.เคยมีทารกตายในครรภ์หรือตายแรกเกิด (1 เดือนแรก)"
    },
    {
      "check": false,
      "question": "2.เคยแท้ง 3 ครั้ง ติดต่อกันหรือมากกว่าติดต่อกัน"
    },
    {
      "check": false,
      "question":
          "3.เคยคลอดบุตรน้ำหนักน้อยกว่า 2,500 กรัม หรือคลอดเมื่ออายุครรภ์น้อยกว่า 37 สัปดาห์ "
    },
    {"check": false, "question": "4.เคยคลอดบุตรน้ำหนักมากกว่า 4,000 กรัม"},
    {
      "check": false,
      "question":
          "5.เคยเข้ารับการรักษาพยาบาลความดันโลหิตสูงระหว่างตั้งครรภ์หรือครรภ์เป็นพิษ"
    },
    {
      "check": false,
      "question":
          "6.เคยผ่าตัดอวัยวะในระบบสืบพันธุ์ เช่น เนื้องอกมดลูก ผ่าตัดคลอด ผูกปากมดลูก ฯลฯ"
    },
    {"section": "ประวัติปัจจุบัน"},
    {
      "check": false,
      "info": {
        "title": "7.ครรภ์แฝด",
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''หมายถึง การตั้งครรภ์ที่เกิดทารกในครรภ์มากกว่า 1 คนขึ้นไป มีความเสี่ยงที่จะเกิดโอกาสแท้งได้มากกว่าครรภ์เดี่ยว เสี่ยงต่อการคลอดก่อนกำหนด เกิดเบาหวาน หรือ ความดันโลหิตสูงขณะตั้งครรภ์ หรือการเกิดภาวะตกเลือดหลังคลอด''')
        ]))
      },
      "question": "7.ครรภ์แฝด"
    },
    {
      "check": false,
      "info": {
        "title": '''8.อายุ<17ปี (อายุน้อยกว่า17ปี)''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''การตั้งครรภ์ในวัยรุ่น หมายถึง การตั้งครรภ์ในหญิงที่มีอายุระหว่าง 10-19 ปี\n เป็นวัยที่ยังต้องศึกษาเล่าเรียน จะทำให้เกิดผลการะทบด้านร่างกาย จิตใจ และสังคม สำหรับมารดาและทารกนั้น บางรายพบ ภาวะครรภ์เป็นพิษ การคลอดก่อนกำหนด  ถุงน้ำคร่ำแตกก่อนกำหนด และทารกแรกเกิดน้ำหนักน้อย''')
        ]))
      },
      "question": "8.อายุ < 17 ปี (นับถึง EDC)"
    },
    {
      "check": false,
      "info": {
        "title": '''9.อายุ ≥ 35 ปี 
(อายุมากกว่าหรือเท่ากับ 35 ปี)''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''การตั้งครรภ์อายุมาก หมายถึง การตั้งครรภ์สตรีที่มีอายุ มากกว่าหรือเท่ากับ 35 ปี 
ทำให้การตั้งครรภ์มีความเสี่ยงที่เป็นปัญหาสุขภาพต่อตนเองและทารกในครรภ์เพิ่มมากขึ้น เช่น การเกิดโรคเบาหวานและความดันโลหิตสูงขณะตั้งครรภ์ รกเกาะต่ำ การแท้ง การคลอดก่อนกำหนดและการเกิดทารกมีภาวะดาวน์ซินโดรม
''')
        ]))
      },
      "question": "9.อายุ ≥ 35 ปี (นับถึง EDC)"
    },
    {
      "check": false,
      "info": {
        "title": '''10.Rh Negative''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''เป็นภาวะของคุณแม่ที่มีหมู่เลือด Rh negative และทารกมีหมู่เลือด Rh positive ซึ่งไม่ตรงกัน ทำให้เกิดปัญหาเสี่ยงที่จะเกิด การแท้งบุตร เด็กตายในครรภ์  การตกเลือดทั้งก่อนและหลังคลอด
''')
        ]))
      },
      "question": "10.Rh Negative"
    },
    {
      "check": false,
      "info": {
        "title": '''11.เลือดออกทางช่องคลอด''',
        "body": RichText(
            text: TextSpan(children: [
          boldTextSpan("เลือดออกจากโพรงมดลูกก่อน 28 สัปดาห์\n"),
          blackTextSpan('สาเหตุอาจมาจากอะไรได้บ้าง\n'),
          boldTextSpan("แท้ง "),
          blackTextSpan(
              '''คือ การสิ้นสุดการตั้งครรภ์ก่อน 28 สัปดาห์ อาจก่อให้เกิดการตกเลือดหรือเสียเลือดกับคุณแม่\n'''),
          boldTextSpan("ท้องนอกมดลูก "),
          blackTextSpan(
              '''คือ ตัวอ่อนไม่ได้ฝังอยู่ในโพรงมดลูก มีอาการ ปวดท้องน้อย กดเจ็บบริเวณหน้าท้อง เลือดออกทางช่องคลอด\n'''),
          boldTextSpan("ครรภ์ไข่ปลาอุก "),
          blackTextSpan(
              '''คือ มีอาการเริ่มแรกเหมือนการตั้งครรภ์ทั่วไป แต่ครรภ์ไข่ปลากอุกมีอาการ ขาดประจำเดือน เลือดออกทางช่องคลอดเป็นสีน้ำตาลหรือสีแดง คลำหน้าท้องไม่พบส่วนของทารก'''),
          boldTextSpan("\n\nเลือดออกจากโพรงมดลูกหลัง 28 สัปดาห์"),
          blackTextSpan('\nสาเหตุอาจมาจากอะไรได้บ้าง'),
          boldTextSpan("\nรกเกาะต่ำ "),
          blackTextSpan(
              "คือ ภาวะที่รักเกาะต่ำกว่าปกติ อาการ เลือดออกทางช่องคลอดลักษณะออกๆหยุดๆ"),
          boldTextSpan("\nรกลอกตัวก่อนกำหนด "),
          blackTextSpan(
              "คือ รกลอกตัวก่อนทารกจะคลอดออกมา ถ้ามีภาวะนี้จะเสี่ยงต่อการตกเลือดรุนแรงหรือทารกอาจเสียชีวิต"),
        ]))
      },
      "question": "11.เลือดออกทางช่องคลอด"
    },
    {
      "check": false,
      "info": {
        "title": '''12.มีก้อนในอุ้งเชิงกราน''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''ถ้าตรวจพบก้อนในอุ้งเชิงกรานขณะตั้งครรภ์   เป็นการสร้างความกังวลใจให้กับคุณแม่และแพทย์ผู้ซึ่งไม่ทราบว่าก้อนเนื้อนั้นจะทำให้เกิดภาวะแทรกซ้อนที่อันตรายต่อร่างกายหรือไม่ หากเป็นก้อนเนื้อธรรมดา มักจะลดขนาดลงและหายไปเอง ส่วนน้อยอาจก่อให้เกิดการขัดขวางการคลอดที่ต้องผ่าตัดออก
''')
        ]))
      },
      "question": "12.มีก้อนในอุ้งเชิงกราน"
    },
    {
      "check": false,
      "info": {
        "title": '''13.ความดันโลหิต 
Diastolic ≥ 90 mmHg''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''หญิงที่มีภาวะความดันโลหิตสูงขณะตั้งครรภ์จะเกิดความเสี่ยงต่อการชัก ภาวะครรภ์เป็นพิษ รกลอกตัวก่อนกำหนด ทารกมีน้ำหนักตัวน้อย ทารกเจริญเติบโตช้า
''')
        ]))
      },
      "question": "13.ความดันโลหิต Diastolic ≥90 mmHg."
    },
    {
      "check": false,
      "info": {
        "title": '''14. BMI < 18.5 กก./ตร.ม. 
หรือ ≥23 กก./ตร.ม.''',
        "body": RichText(
            text: TextSpan(children: [
          boldTextSpan("BMI "),
          blackTextSpan(
              " คือ ค่าดัชนีที่ใช้วัดความสมดุลของน้ำหนักตัวและส่วนสูง ซึ่งมาจากนำน้ำหนักตัวหารส่วนสูง(เมตร)ยกกำลังสอง BMI ก่อนการตั้งครรภ์มีค่าปกติ 18.5-24.9 กก/ตร.ม.สำหรับชาวเอเชียควรน้อยกว่า 23 กก/ตร.ม.\n\n"),
          boldTextSpan("น้อย"),
          blackTextSpan(
              ' (BMI < 18.5 กก./ตร.ม.) ขณะตั้งครรภ์ควรมีน้ำหนักเพิ่มขึ้น 12.5-18 กก. เสี่ยงที่จะเกิดผลกระทบต่อการตั้งครรภ์ เช่น ทารกขาดสารอาหาร ทารกเจริญเติบโตช้า โลหิตจาง\n\n'),
          boldTextSpan("เกิน"),
          blackTextSpan(
              ' (BMI ≥23 กก./ตร.ม.) ขณะตั้งครรภ์ควรมีน้ำหนักเพิ่มขึ้น 11.5-16 กก. เสี่ยงที่จะเกิดผลกระทบต่อการตั้งครรภ์ เช่น เบาหวานและความดันโลหิตสูงขณะตั้งครรภ์ การคลอดติดไหล่\n\n'),
        ]))
      },
      "question": "14. BMI < 18.5 กก./ตร.ม. หรือ ≥ 23 กก./ตร.ม."
    },
    {"section": "ประวัติทางอายุรกรรม"},
    {
      "check": false,
      "info": {
        "title": '''''',
        "body": RichText(
            text: TextSpan(children: [
          boldTextSpan("โลหิตจางจากการขาดธาตุเหล็ก"),
          blackTextSpan(
              ''' เกิดจากการรับประทานยาและรับประทานอาหารที่มีส่วนผสมของธาตุเหล็กไม่เพียงพอต่อความต้องการของร่างกาย ส่งผลกระทบต่อการตั้งครรภ์ คุณแม่จะมีโอกาสติดเชื้อง่าย ภูมิคุ้มกันลดลง ทารกเจริญเติบโตช้า น้ำหนักน้อยเมื่อแรกเกิด อาจคลอดก่อนกำหนด'''),
          boldTextSpan("\n\nโลหิตจางจากโรคธาลัสซีเมีย"),
          blackTextSpan(
              " ซึ่งเป็นโรคถ่ายทอดทางพันธุกรรม และทำให้เม็ดเลือดแดงถูกทำลายได้ง่าย ส่งผลให้คุณแม่และทารก เสี่ยงต่อการคลอดก่อนกำหนด ทารกแรกเกิดมีน้ำหนักน้อย ตกเลือดหลังคลอด")
        ]))
      },
      "question": "15.โลหิตจาง"
    },
    {
      "check": false,
      "info": {
        "title": '''''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''ภาวะเบาหวานขณะตั้งครรภ์ เพิ่มความเสี่ยงต่อการเกิดความดันโลหิตสูง การคลอดยาก การตกเลือดก่อนและหลังคลอด รวมทั้งคุณแม่และทารกมีโอกาสเกิดโรคเบาหวานได้''')
        ]))
      },
      "question": "16.โรคเบาหวาน"
    },
    {
      "check": false,
      "info": {
        "title": '''''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''หากคุณแม่เป็นโรคไต มีโอกาสเกิดการเจริญเติบโตช้า รกลอกตัวก่อนกำหนด ความดันโลหิตสูง ชัก''')
        ]))
      },
      "question": "17.โรคไต"
    },
    {
      "check": false,
      "info": {
        "title": '''''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''ผลกระทบจะเกิดขึ้นในขณะตั้งครรภ์ มีโอกาสจะเกิดการแท้งเพิ่มขึ้น คลอดก่อนกำหนด ตกเลือดหลังคลอด ทารกโตช้าในครรภ์''')
        ]))
      },
      "question": "18.โรคหัวใจ"
    },
    {
      "check": false,
      "info": {
        "title": '''''',
        "body": RichText(
            text: TextSpan(children: [
          blackTextSpan(
              '''พบว่าภาวะแทรกซ้อนที่มีโอกาสเกิดขึ้น ได้แก่ ทารกน้ำหนักตัวน้อย การเจริญเติบโตช้า และภาวะตัวเหลือง''')
        ]))
      },
      "question": "19.ติดยาเสพติด ติดสุรา สูบบุหรี่ คนใกล้ชิดสูบบุหรี่"
    },
    {"check": false, "question": "20.โรคอายุรกรรม อื่นๆ ไทรอยด์ SLE ฯลฯ"},
  ];

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: " การเตรียมตัวไปฝากครรภ์",
      widgets: [
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  pageController = new PageController(initialPage: 0);
                  showDialogPage("ทำไมต้องไปฝากครรภ์เร็ว", 0);
                },
                child: "ทำไมต้องไปฝากครรภ์เร็ว".text.size(16).white.make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  pageController = new PageController(initialPage: 1);
                  showDialogPage("ขั้นตอนการฝากครรภ์", 1);
                },
                child: "ขั้นตอนการฝากครรภ์"
                    .text
                    .align(TextAlign.center)
                    .size(16)
                    .white
                    .make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  pageController = new PageController(initialPage: 2);
                  showDialogPage("การคัดกรองความเสี่ยงต่าง ๆ ของคุณแม่", 2);
                },
                child: "การคัดกรองความเสี่ยงต่าง ๆ ของคุณแม่"
                    .text
                    .align(TextAlign.center)
                    .size(16)
                    .white
                    .make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
      ],
    );
  }

  showDialogPage(String title, int index) =>
      Get.to(() => StatefulBuilder(builder: (context, setState) {
            List widgets = [
              ImagePreview(
                imagePath: "assets/images/step_to_anneltal1.png",
                withBackButton: false,
              ).box.make().marginAll(8).cornerRadius(15).p8(),
              SingleChildScrollView(
                child: Column(children: page2()).p8(),
              ).box.white.make().cornerRadius(15).marginAll(8),
              SingleChildScrollView(
                child: Column(children: page3(setState)).p8(),
              ).box.white.make().cornerRadius(15).marginAll(8)
            ];
            return Container(
              height: Get.height,
              width: Get.width,
              child: Scaffold(
                  backgroundColor: MainColors.pink,
                  appBar: AppBar(
                    title: "$title".text.white.fontFamily('prompt').make(),
                    centerTitle: true,
                    backgroundColor: MainColors.pink,
                    shadowColor: Colors.transparent,
                  ),
                  body: widgets[index]),
            ).material();
          }));

  List<Widget> page2() {
    return [
      "ขั้นตอนการฝากครรภ์".text.size(18).bold.make(),
      Container(
              height: Get.height * .3,
              width: Get.width,
              child: FlutterYoutubeView(
                  onViewCreated: (ctr) {},
                  listener: null,
                  scaleMode:
                      YoutubeScaleMode.none, // <option> fitWidth, fitHeight
                  params: YoutubeParam(
                      videoId: '$youtube1',
                      showUI: true,
                      showYoutube: true,
                      startSeconds: 0.0, // <option>
                      autoPlay: false) // <option>
                  ))
          .marginOnly(bottom: 8),
      "เปิดบน Youtube"
          .text
          .white
          .makeCentered()
          .box
          .width(Get.width * .95)
          .height(30)
          .red500
          .make()
          .card
          .make()
          .onTap(() {
        launch("https://www.youtube.com/watch?v=$youtube1");
      }),
      "การเตรียมตัวมาฝากครรภ์".text.size(18).bold.make(),
      Container(
              height: Get.height * .3,
              width: Get.width,
              child: FlutterYoutubeView(
                  onViewCreated: (ctr) {},
                  listener: null,
                  scaleMode:
                      YoutubeScaleMode.none, // <option> fitWidth, fitHeight
                  params: YoutubeParam(
                      videoId: '$youtube2',
                      showUI: true,
                      showYoutube: true,
                      startSeconds: 0.0, // <option>
                      autoPlay: false) // <option>
                  ))
          .marginOnly(bottom: 8),
      "เปิดบน Youtube"
          .text
          .white
          .makeCentered()
          .box
          .width(Get.width * .95)
          .height(30)
          .red500
          .make()
          .card
          .make()
          .onTap(() async {
        launch("https://www.youtube.com/watch?v=$youtube2");
      }),
    ];
  }

  List<Widget> page3(setState) =>
      [
        RichText(
            text: TextSpan(children: [
          boldTextSpan("คำชี้แจง :"),
          blackTextSpan(
              " ข้อความต่อไปนี้เป็นการคัดกรองความเสี่ยงต่าง ๆ ของคุณแม่ หากมีตามข้อดังกล่าวให้ทำเครื่องหมาย ✓ ช่อง หากไม่มีให้ปล่อยว่างไว้")
        ])).marginOnly(bottom: 10),
        RichText(
            text: TextSpan(children: [
          boldTextSpan("หมายเหตุ :"),
          blackTextSpan(
              " ถ้าคุณแม่อยากรู้ถึงความเสี่ยงในข้อ 7-19 สามารถแตะที่ตัวอักษรสีฟ้าเพื่ออ่านรายละเอียดแต่ละข้อได้")
        ])).marginOnly(bottom: 10),
      ] +
      List<Widget>.generate(questionList.length, (index) {
        final item = questionList[index];
        return item['section'] != null
            ? "${item['section']}".text.bold.make().marginSymmetric(vertical: 8)
            : Card(
                color: item['check'] ? Colors.blue[50] : Colors.white,
                child: ListTile(
                  onTap: () => item['info'] == null
                      ? null
                      : showAlertDialog(
                          item['question'], [item['info']['body']]),
                  title: "${item['question']} "
                      .text
                      .size(14)
                      .color(item['info'] == null ? Colors.black : Colors.blue)
                      .make(),
                  trailing: Checkbox(
                    onChanged: (value) => setState(() => item['check'] = value),
                    value: item['check'],
                    activeColor: MainColors.blue,
                  ),
                ),
              );
      }).toList() +
      [
        MaterialButton(
          onPressed: () {
            Get.back();
            final result = questionList
                .where((q) => q['question'] != null && q['check'])
                .toList();
            if (result.length > 0) {
              showAlertDialog(
                  "เป็นการตั้งครรภ์ความเสี่ยงสูง",
                  [
                    "ท่านต้องได้รับการส่งต่อเพื่อรับการดูแล ประเมินเพิ่มเติมจากสูติแพทย์"
                        .text
                        .make()
                  ],
                  onConfirm: () => Get.off(() => HomePage()),
                  textConfirm: "กลับหน้าหลัก",
                  titleBgColor: Colors.red);
            } else {
              showAlertDialog(
                  "เป็นการตั้งครรภ์ความเสี่ยงต่ำ",
                  [
                    "แนะนำให้คอยสังเกตอาการตัวเอง หากพบความเสี่ยง ท่านต้องได้รับการตรวจและประเมินเพิ่มเติมจากสูติแพทย์"
                        .text
                        .make(),
                  ],
                  onConfirm: () => Get.off(() => HomePage()),
                  textConfirm: "กลับหน้าหลัก",
                  titleBgColor: Colors.green);
            }
          },
          minWidth: Get.width,
          height: 40,
          color: MainColors.blue,
          child: "คลิกแปลผล".text.white.make(),
        )
      ];
}
