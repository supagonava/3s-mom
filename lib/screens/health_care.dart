import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:s3mom/widgets/page_template.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:get/get.dart';

class HealthCarePage extends StatefulWidget {
  const HealthCarePage({Key key}) : super(key: key);

  @override
  _HealthCarePageState createState() => _HealthCarePageState();
}

class _HealthCarePageState extends State<HealthCarePage> {
  int page1 = 0;
  int page2 = 0;
  int page3 = 0;

  Map<String, Map> sounds = {
    "firstTrimas": {
      0: "assets/sounds/t1/1.mp3",
      1: "assets/sounds/t1/2.mp3",
      2: "assets/sounds/t1/3.mp3",
      3: "assets/sounds/t1/4.mp3",
      4: "assets/sounds/t1/5.mp3",
      5: "assets/sounds/t1/6.mp3",
      6: "assets/sounds/t1/7.mp3",
    },
    "trimas23": {
      0: "assets/sounds/t2/t2-1.mp3",
      1: "assets/sounds/t2/t2-2.mp3",
      2: "assets/sounds/t2/t2-3.mp3",
      3: "assets/sounds/t2/t2-4.mp3",
      4: "assets/sounds/t2/t2-5.mp3",
      5: "assets/sounds/t2/t2-6.mp3",
      6: "assets/sounds/t2/t2-71.mp3",
      7: "assets/sounds/t2/t2-72.mp3",
      8: "assets/sounds/t2/t2-81.mp3",
      9: "assets/sounds/t2/t2-82.mp3",
    },
    "prepare": {
      0: "assets/sounds/prepare_to_pregnant/denoised.mp3",
      1: "assets/sounds/prepare_to_pregnant/denoised.mp3"
    }
  };

  List<String> firstTrimas = [
    "assets/images/takecare/firsttrimas/firsttrimas 1.png",
    "assets/images/takecare/firsttrimas/firsttrimas 2.png",
    "assets/images/takecare/firsttrimas/firsttrimas 3.png",
    "assets/images/takecare/firsttrimas/firsttrimas 4.1.png",
    "assets/images/takecare/firsttrimas/firsttrimas 4.2.png",
    "assets/images/takecare/firsttrimas/firsttrimas 5.png",
    "assets/images/takecare/firsttrimas/firsttrimas 6.jpg",
  ];

  List<String> trimas23 = [
    "assets/images/takecare/trimas23/1ตกขาว.png",
    "assets/images/takecare/trimas23/2จุกลิ้นปี่.png",
    "assets/images/takecare/trimas23/3tongpung.png",
    "assets/images/takecare/trimas23/4เท้าบวม.png",
    "assets/images/takecare/trimas23/5ปวดหลัง.png",
    "assets/images/takecare/trimas23/6มดลูกหดรัดตัว.png",
    "assets/images/takecare/trimas23/7นอนไม่หลับ.png",
    "assets/images/takecare/trimas23/7นอนไม่หลับ(ต่อ).png",
    "assets/images/takecare/trimas23/8อารมณ์แปรปรวน.png",
    "assets/images/takecare/trimas23/8อารมณ์แปรปรวน(ต่อ).png",
  ];

  List<String> prepare = [
    "assets/images/takecare/prepare/1.JPG",
    "assets/images/takecare/prepare/2.JPG",
  ];

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: "การดูแลตัวเอง",
      widgets: [
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () => showScrollView(context,
                    "การดูแลตนเองในไตรมาสแรก (3 เดือนแรก)", firstTrimas,
                    sounds: sounds['firstTrimas']),
                child: "การดูแลตนเองในไตรมาสแรก (3 เดือนแรก)"
                    .text
                    .size(14)
                    .white
                    .make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () => showScrollView(context,
                    "การดูแลตนเองในไตรมาส 2-3 (3 เดือนขึ้นไป)", trimas23,
                    sounds: sounds['trimas23']),
                child: "การดูแลตนเองในไตรมาส 2-3 (3 เดือนขึ้นไป)"
                    .text
                    .align(TextAlign.center)
                    .size(14)
                    .white
                    .make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () => showScrollView(
                    context, "การเตรียมตัวคลอด", prepare,
                    sounds: sounds['prepare']),
                child: "การเตรียมตัวคลอด"
                    .text
                    .align(TextAlign.center)
                    .size(14)
                    .white
                    .make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
      ],
    );
  }
}
