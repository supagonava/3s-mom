import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/screens/home_page.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:s3mom/widgets/page_template.dart';
import 'package:velocity_x/velocity_x.dart';

class CheckListPregnant extends StatefulWidget {
  const CheckListPregnant({Key key}) : super(key: key);

  @override
  _CheckListPregnantState createState() => _CheckListPregnantState();
}

class _CheckListPregnantState extends State<CheckListPregnant> {
  ScrollController scrollController = ScrollController();
  int page = 1;
  List<Map> questionList = [
    {
      'no': 1,
      'question':
          "มีเพศสัมพันธ์กับแฟนแล้วประจำเดือน(เมนส์)ไม่มาตามกำหนดมากกว่า 5 วัน",
      'info': {
        'title': 'อาการประจำเดือนขาด หรือ เมนส์ไม่มา',
        'body':
            '''   โดยทั่วไปผู้หญิงที่แต่งงานแล้วและมีร่างกายแข็งแรงสมบูรณ์ดี มีประจำเดือนมาสม่ำเสมอ เมื่อประจำเดือนไม่มาก็สงสัยว่าน่าจะตั้งครรภ์ โดยเฉพาะถ้าประจำเดือนไม่มานานเกินกว่า กำหนด 5 วัน นั่นอาจเป็นสัญญาณที่บอกได้ว่า “กำลังตั้งครรภ์”     อย่างไรก็ตามการขาดของประจำเดือนก็อาจมาจากสาเหตุอื่นที่ไม่ใช่การตั้งครรภ์ก็ได้ เช่น อยู่ในช่วงไข่ตก การใช้ยาคุมกำเนิด ร่างกายได้รับสารอาหารไม่เพียงพอ รู้สึกเครียดมากจนเกินไป หรือเป็นโรคบางอย่าง เช่น โรคของระบบต่อมไร้ท่อ เบาหวาน โรคเกี่ยวกับรังไข่ เป็นต้น '''
      },
      'checked': false
    },
    {
      'no': 2,
      'question': 'คัดตึงเต้านม/เต้านมขยายใหญ่ขึ้น ลานนมมีสีเข้มขึ้น',
      'info': {
        'title': 'การเปลี่ยนแปลงของเต้านม',
        'body':
            '''  คุณแม่ที่กำลังตั้งครรภ์จะมีความรู้สึกถึงการเปลี่ยนแปลงของเต้านม เช่น คัดตึงเต้านม เริ่มตั้งแต่อายุครรภ์ 6 สัปดาห์ขึ้นไป เนื่องจากเกิดการกระตุ้นของฮอร์โมนชนิดหนึ่ง ทางการแพทย์เรียกว่า “ฮอร์โมนเอสโตรเจน” ทำให้เกิดการสร้างต่อมน้ำนม เต้านมขยายใหญ่ ลานนมมีสีเข้มขึ้น
'''
      },
      'checked': false
    },
    {
      'no': 3,
      'question': 'คลื่นไส้ อาเจียนหรือแพ้ท้อง',
      'info': {
        'title': 'อาการคลื่นไส้ (อาจมีอาเจียนร่วมด้วย)',
        'body':
            '''    ในระหว่างที่คุณแม่กำลังตั้งครรภ์จะเกิดการเปลี่ยนแปลงของฮอร์โมน ทำให้รบกวนการทำงานของระบบทางเดินอาหารได้บ่อยๆ เป็นผลให้มีอาการคลื่นไส้อาเจียน นอกจากนี้ยังทำให้เกิดอาการท้องผูก ท้องอืด รู้สึกไม่สบายท้องได้อีกด้วย
   อีกสาเหตุหนึ่งที่ทำให้เกิดอาการแพ้ท้องก็คืออาการที่จมูกจะไวต่อกลิ่นทุกชนิดมากเป็นพิเศษ บางรายมีอาการเหม็นกลิ่นอาหาร กลิ่นตัวสามี เป็นต้น ซึ่งเป็นกลไกตามธรรมชาติที่จะพยายามป้องกันร่างกายของคุณแม่ที่กำลังตั้งครรภ์ นอกจากนี้สภาวะอารมณ์ก็ส่งผลต่ออาการคลื่นไส้ เช่น มีความเครียด วิตกกังวล
'''
      },
      'checked': false
    },
    {
      'no': 4,
      'question': 'มีความรู้สึกว่าปัสสาวะบ่อยขึ้น',
      'info': {
        'title': 'ปัสสาวะบ่อย',
        'body':
            '''    ในช่วงไตรมาสแรกของการตั้งครรภ์ หรือประมาณ 3 เดือนแรก คุณแม่จะรู้สึกอยากปัสสาวะบ่อย เกิดจากการที่มดลูกมีขนาดโตขึ้นจนไปเบียดกับกระเพาะปัสสาวะ
    แต่ในคนทั่วไปที่มีการติดเชื้อของทางเดินปัสสาวะ เนื้องอกในเชิงกราน เป็นโรคเบาหวาน หรือมีภาวะเครียดก็เป็นสาเหตุของการปัสสาวะบ่อยได้ 
'''
      },
      'checked': false
    },
    {
      'no': 5,
      'question': 'เหนื่อย อ่อนเพลีย และง่วงนอน',
      'info': {
        'title': 'อาการอ่อนเพลีย',
        'body':
            '''    คุณแม่ที่ตั้งครรภ์ในช่วงไตรมาสแรกหรือประมาณ 3 เดือนแรกจะรู้สึกอ่อนเพลียและง่วงนอน เป็นเพราะในระหว่างตั้งครรภ์ร่างกายจะใช้พลังงานมากกว่าปกติ อีกทั้งทารกที่กำลังเจริญเติบโตในครรภ์มีความต้องการออกซิเจนจากคุณแม่ จึงส่งผลให้คุณแม่มีอาการหายใจถี่ เหนื่อย และอ่อนเพลียได้
'''
      },
      'checked': false
    },
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return PageTemplate(
      title: "การประเมินการตั้งครรภ์",
      widgets: [
        page == 1
            ? Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        border: Border.all(color: Colors.black12)),
                    width: size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                            text: TextSpan(children: [
                          "คำชี้แจง :"
                              .textSpan
                              .black
                              .fontFamily('prompt')
                              .size(16)
                              .bold
                              .make(),
                          " ข้อความต่อไปนี้เป็นอาการที่สงสัยว่าคุณจะตั้งครรภ์ ขอให้คุณสังเกตตัวเองและหากมีอาการดังกล่าวให้ทำเครื่องหมาย  ✓  ในข้อนั้น (เลือกได้มากกว่า 1 ข้อ)"
                              .textSpan
                              .black
                              .fontFamily('prompt')
                              .size(16)
                              .make(),
                        ])),
                        RichText(
                            text: TextSpan(children: [
                          "หมายเหตุ : "
                              .textSpan
                              .black
                              .fontFamily('prompt')
                              .size(16)
                              .bold
                              .make(),
                          "ถ้าคุณแม่ต้องการทราบเกี่ยวกับสาเหตุของอาการเพิ่มเติมให้คลิ๊กที่ "
                              .textSpan
                              .black
                              .fontFamily('prompt')
                              .size(16)
                              .make(),
                          "อ่านเพิ่มเติม"
                              .textSpan
                              .amber500
                              .bold
                              .underline
                              .wide
                              .fontFamily('prompt')
                              .make()
                        ])),
                      ],
                    ),
                  ),
                  Column(
                    children: List<StatelessWidget>.generate(
                      questionList.length,
                      (index) => Card(
                        color: questionList[index]['checked']
                            ? Colors.blue[50]
                            : Colors.white,
                        child: ListTile(
                          onTap: () => showAlertDialog(
                              questionList[index]['info']['title'], [
                            "${questionList[index]['info']['body']}".text.make()
                          ]),
                          title: RichText(
                              text: TextSpan(children: [
                            "${index + 1}. ${questionList[index]['question']} "
                                .textSpan
                                .black
                                .fontFamily("Prompt")
                                .make(),
                            "อ่านเพิ่มเติม"
                                .textSpan
                                .amber500
                                .underline
                                .wide
                                .fontFamily('prompt')
                                .make()
                          ])),
                          trailing: Checkbox(
                            onChanged: (value) => setState(
                                () => questionList[index]['checked'] = value),
                            value: questionList[index]['checked'],
                            activeColor: MainColors.blue,
                          ),
                        ),
                      ),
                    ).toList(),
                  ),
                  MaterialButton(
                      minWidth: size.width,
                      color: MainColors.blue,
                      child: Text("ดูผลลัพธ์",
                          style: TextStyle(fontSize: 16, color: Colors.white)),
                      onPressed: () => Get.defaultDialog(
                          title: "ผลลัพธ์จากการประเมิน",
                          titleStyle: TextStyle(color: Colors.white),
                          backgroundColor: MainColors.pink,
                          content:
                              "หากมีข้อใดข้อหนึ่งข้างต้น คุณแม่อาจจะตั้งท้อง ให้คุณแม่ทดสอบการตั้งท้อง โดยไปซื้อชุดตรวจจากร้านขายยาทั่วไปมาตรวจ หรือไปตรวจที่โรงพยาบาลใกล้บ้าน"
                                  .text
                                  .center
                                  .makeCentered()
                                  .box
                                  .color(Vx.white)
                                  .height(150)
                                  .width(Get.width)
                                  .p8
                                  .make(),
                          textConfirm: "ถัดไป",
                          confirmTextColor: Colors.white,
                          onConfirm: () {
                            setState(() => page = 2);
                            Get.back();
                          })),
                ],
              )
            : page == 2
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                          text: TextSpan(children: [
                        "ชุดตรวจครรภ์".textSpan.bold.black.size(18).make(),
                        " มี 3 แบบ ได้แก่".textSpan.size(16).black.make(),
                      ])).p8(),
                      Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            border: Border.all(color: Colors.black12)),
                        width: size.width,
                        height: 220,
                        child: Stack(
                          children: [
                            NotificationListener(
                              onNotification: (noti) {
                                setState(() =>
                                    print(scrollController.position.pixels));
                                return true;
                              },
                              child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  controller: scrollController,
                                  children: [
                                    Center(
                                      child: Column(
                                        children: [
                                          ImageView(
                                            height: 180,
                                            width: 180,
                                            imagePath:
                                                "assets/images/tool1.png",
                                            isAsset: true,
                                          ),
                                          RichText(
                                            text: TextSpan(children: [
                                              "แบบ"
                                                  .textSpan
                                                  .black
                                                  .fontFamily('prompt')
                                                  .make(),
                                              "จุ่มปัสสาวะ"
                                                  .textSpan
                                                  .red500
                                                  .fontFamily('prompt')
                                                  .make(),
                                            ]),
                                          )
                                        ],
                                      ),
                                    ),
                                    Center(
                                      child: Column(
                                        children: [
                                          ImageView(
                                            height: 180,
                                            width: 180,
                                            imagePath:
                                                "assets/images/tool2.png",
                                            isAsset: true,
                                          ),
                                          RichText(
                                            text: TextSpan(children: [
                                              "แบบ"
                                                  .textSpan
                                                  .black
                                                  .fontFamily('prompt')
                                                  .make(),
                                              "หยดปัสสาวะใสชุ่ดตรวจ"
                                                  .textSpan
                                                  .red500
                                                  .fontFamily('prompt')
                                                  .make(),
                                            ]),
                                          )
                                        ],
                                      ),
                                    ),
                                    Center(
                                      child: Column(
                                        children: [
                                          ImageView(
                                            height: 180,
                                            width: 180,
                                            imagePath:
                                                "assets/images/tool3.png",
                                            isAsset: true,
                                          ),
                                          RichText(
                                            text: TextSpan(children: [
                                              "แบบ"
                                                  .textSpan
                                                  .black
                                                  .fontFamily('prompt')
                                                  .make(),
                                              "แบบปัสสาวะผ่าน"
                                                  .textSpan
                                                  .red500
                                                  .fontFamily('prompt')
                                                  .make(),
                                            ]),
                                          )
                                        ],
                                      ),
                                    ),
                                  ]),
                            ),
                            Positioned(
                                top: 80,
                                child: scrollController.hasClients &&
                                        scrollController.position.pixels != 0
                                    ? CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: Icon(Icons.arrow_left,
                                            size: 40, color: MainColors.pink),
                                      ).onTap(() {
                                        scrollController.animateTo(
                                            scrollController.position.pixels -
                                                Get.width * .4,
                                            duration: Duration(seconds: 1),
                                            curve: Curves.ease);
                                      })
                                    : Container()),
                            Positioned(
                                top: 80,
                                right: 0,
                                child: !scrollController.hasClients ||
                                        (scrollController.hasClients &&
                                            scrollController
                                                    .position.maxScrollExtent !=
                                                scrollController
                                                    .position.pixels)
                                    ? CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: Icon(Icons.arrow_right,
                                            size: 40, color: MainColors.pink),
                                      ).onTap(() {
                                        scrollController.animateTo(
                                            scrollController.position.pixels +
                                                Get.width * .4,
                                            duration: Duration(seconds: 1),
                                            curve: Curves.ease);
                                      })
                                    : Container()),
                          ],
                        ),
                      ),
                      RichText(
                          text: TextSpan(children: [
                        "**การเลือกซื้อชุดตรวจครรภ์ "
                            .textSpan
                            .red500
                            .fontFamily('prompt')
                            .size(16)
                            .bold
                            .make(),
                        "ควรเลือกอุปกรณ์ที่มี อย. ยังไม่หมดอายุการใช้งาน อ่านวิธีการตรวจและทาตามขั้นตอนที่แนะนาข้างกล่อง"
                            .textSpan
                            .black
                            .fontFamily('prompt')
                            .size(16)
                            .make(),
                      ])).pOnly(top: 8),
                      Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          MaterialButton(
                                  color: MainColors.pink,
                                  onPressed: () => setState((() => page = 1)),
                                  child: "ย้อนกลับ".text.white.make())
                              .box
                              .width(100)
                              .make(),
                          MaterialButton(
                                  color: MainColors.pink,
                                  onPressed: () => setState((() => page = 3)),
                                  child: "ถัดไป".text.white.make())
                              .box
                              .width(100)
                              .make()
                        ],
                      ),
                    ],
                  )
                : Column(
                    children: [
                      "การแปลผลการทดสอบการตั้งท้อง"
                          .text
                          .headline6(context)
                          .makeCentered(),
                      RichText(
                          text: TextSpan(children: [
                        blackTextSpan("   อุปกรณ์ทุกชนิด "),
                        redTextSpan(
                            "เมื่อโดนปัสสาวะ จะเปลี่ยนสี 1ขีด เสมอ ตรงแถบ C "),
                        blackTextSpan(
                            "แสดงว่าได้ผลลบ หรือ “น่าจะไม่ตั้งครรภ์” (ดังรูป)"),
                        redTextSpan(
                            "  หากไม่พบสีใด ๆ แสดงว่า อุปกรณ์ตรวจใช้งานไม่ได้หรือตรวจไม่ถูกวิธี")
                      ])).pOnly(top: 8),
                      ImageView(
                        height: 100,
                        isAsset: true,
                        imagePath: "assets/images/result1tab.png",
                      ),
                      "   หากตรวจพบสี 2 ขีด ตรงแถบ C และ T แสดงว่า ได้ผลตรวจเป็นบวก หรือ “น่าจะตั้งครรภ์”"
                          .text
                          .size(16)
                          .black
                          .make(),
                      ImageView(
                        height: 100,
                        isAsset: true,
                        imagePath: "assets/images/result2tab.png",
                      ),
                      Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          MaterialButton(
                                  color: MainColors.pink,
                                  onPressed: () => setState((() => page = 2)),
                                  child: "ย้อนกลับ".text.white.make())
                              .box
                              .width(100)
                              .make(),
                          MaterialButton(
                                  color: MainColors.pink,
                                  onPressed: showResultSelectDialog,
                                  child: "ถัดไป".text.white.make())
                              .box
                              .width(100)
                              .make()
                        ],
                      ),
                    ],
                  ),
      ],
    );
  }

  void showResultSelectDialog() => Get.defaultDialog(
      title: "คลิกข้อควรทำเมื่อตรวจได้ผลลบหรือบวก",
      titleStyle: TextStyle(color: Colors.red),
      content: Container(
        child: Column(
          children: [
            MaterialButton(
                    minWidth: Get.width,
                    height: 60,
                    onPressed: showPositiveResultDialog,
                    child: "ผลเป็นบวก".text.white.make(),
                    color: Colors.green)
                .marginOnly(bottom: 8),
            MaterialButton(
              minWidth: Get.width,
              height: 60,
              onPressed: showNegetiveResultDialog,
              child: "ผลเป็นลบ".text.white.make(),
              color: Colors.red,
            )
          ],
        ),
        height: 150,
        width: Get.width,
      ));

  void showNegetiveResultDialog() {
    Get.back();
    Get.defaultDialog(
        title: "เมื่อผลตรวจเป็น \"ลบ\"",
        content: Container(
            height: 150,
            width: Get.width,
            child:
                "1.คุณอาจจะทดสอบการตั้งครรภ์เร็วเกินไป ให้รออีก 3-5 วันแล้ว ซื้อชุดตรวจครรภ์มาตรวจซ้ำ \n\n2.อุปกรณ์อาจจะเสีย ให้เปลี่ยนอุปกรณ์ ตรวจใหม่ \n\n3.หากมีข้อสงสัยให้ไปเมนู “แชทกับเรา”"
                    .text
                    .make()),
        confirmTextColor: Colors.white,
        textConfirm: "กลับหน้าหลัก",
        onConfirm: () => Get.off(() => HomePage()));
  }

  void showPositiveResultDialog() {
    Get.back();
    Get.defaultDialog(
        title: "เมื่อผลตรวจเป็น \"บวก\"",
        content: Container(
            height: 100,
            width: Get.width,
            child:
                "1.แนะนำให้ไปโรงพยาบาล หรือศูนย์ สุขภาพใกล้บ้าน หรือ ยืนยันการตั้งท้อง และฝากท้อง \n\n2.หากมีข้อสงสัยให้ไปเมนู “แชทกับเรา”"
                    .text
                    .make()),
        confirmTextColor: Colors.white,
        textConfirm: "กลับหน้าหลัก",
        onConfirm: () => Get.off(() => HomePage()));
  }
}
