import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:get/get.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

class ScrollViewContents extends StatefulWidget {
  final String title;
  final List<String> paths;
  final Map sounds;
  final int initPage;

  const ScrollViewContents(
      {Key key, this.title, this.paths, this.initPage, this.sounds})
      : super(key: key);

  @override
  _ScrollViewContentsState createState() => _ScrollViewContentsState();
}

class _ScrollViewContentsState extends State<ScrollViewContents> {
  int page = 1;
  AssetsAudioPlayer audioPlayer = AssetsAudioPlayer();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 500)).then((_) => Get.defaultDialog(
        barrierDismissible: false,
        title: "แนะนำ",
        content: widget.paths.length == 1
            ? "คุณแม่สามารถใช้ 2 นิ้วในการซูม เข้า-ออก บนรูปภาพ"
                .text
                .make()
                .box
                .height(150)
                .width(Get.width)
                .make()
            : "คุณแม่สามารถใช้ 2 นิ้วในการซูม เข้า-ออก บนรูปภาพ และสามารถปัดซ้ายขวาเพื่อดูหน้าอื่น ๆ ได้นะคะ"
                .text
                .make()
                .box
                .height(150)
                .width(Get.width)
                .make(),
        confirmTextColor: Colors.white,
        onConfirm: () => Get.back()));

    if (widget.sounds != null && widget.sounds.length > 0) {
      Future.delayed(Duration(milliseconds: 500)).then((_) => Get.defaultDialog(
          barrierDismissible: false,
          title: "แนะนำ",
          content: Column(children: [
            "คุณแม่สามารถกดที่สัญลักษณ์"
                .text
                .align(TextAlign.center)
                .makeCentered(),
            Icon(Icons.music_note, color: MainColors.blue),
            "ที่มุมขวาบนของจอเพื่อฟังข้อมูลได้นะคะ"
                .text
                .align(TextAlign.center)
                .makeCentered()
          ]).box.height(150).width(Get.width).make(),
          confirmTextColor: Colors.white,
          onConfirm: () => Get.back()));
    }

    audioPlayer.setVolume(1);
  }

  @override
  void dispose() {
    super.dispose();
    audioPlayer.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PageController controller =
        new PageController(initialPage: widget.initPage ?? 0);

    return Scaffold(
      backgroundColor: MainColors.pink,
      appBar: AppBar(
        title: "${widget.title}"
            .text
            .white
            .fontFamily('prompt')
            .size(15)
            .make()
            .p8(),
        actions: [
          (() {
            if (widget.sounds != null) {
              return StreamBuilder(
                builder: (context, AsyncSnapshot snapShot) {
                  if (snapShot.data != null && !snapShot.data) {
                    return CircleAvatar(
                            backgroundColor: Colors.white,
                            child:
                                Icon(Icons.music_note, color: MainColors.blue))
                        .onTap(() {
                      audioPlayer.open(Audio(widget.sounds[page - 1]));
                    });
                  }
                  return CircleAvatar(
                          backgroundColor: MainColors.blue,
                          child: Icon(Icons.music_off, color: Colors.white))
                      .onTap(() {
                    audioPlayer.stop();
                  });
                },
                stream: audioPlayer.isPlaying,
              ).paddingOnly(right: 8);
            }
            return Container();
          }()),
        ],
        centerTitle: true,
        backgroundColor: MainColors.pink,
        shadowColor: Colors.transparent,
      ),
      body: Column(
        children: [
          Container(
            height: Get.height * .75,
            width: Get.width * .9,
            color: Colors.white,
            child: PageView.builder(
              controller: controller,
              onPageChanged: (newPage) {
                if (widget.sounds != null) {
                  print(
                      "compare ${widget.sounds[newPage]} , ${widget.sounds[page - 1]}");
                  if (widget.sounds[newPage] != widget.sounds[page - 1]) {
                    audioPlayer.stop();
                  }
                }
                page = newPage + 1;
                setState(() => print(page));
              },
              itemBuilder: (context, at) {
                if ("${widget.paths[at]}".startsWith("assets")) {
                  return ImagePreview(
                    isAsset: true,
                    imagePath: "${widget.paths[at]}",
                    withBackButton: false,
                  ).centered();
                }
                return Column(children: [
                  Container(
                      height: Get.height * .3,
                      width: Get.width,
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.only(bottom: 8),
                      child: FlutterYoutubeView(
                          onViewCreated: (ctr) {},
                          listener: null,
                          scaleMode: YoutubeScaleMode.fitHeight,
                          params: YoutubeParam(
                              videoId: '${widget.paths[at]}',
                              showUI: true,
                              showYoutube: true,
                              startSeconds: 0.0, // <option>
                              autoPlay: false) // <) // <option>
                          )),
                  "เปิดบน Youtube"
                      .text
                      .white
                      .makeCentered()
                      .box
                      .width(Get.width * .95)
                      .height(30)
                      .red500
                      .make()
                      .card
                      .make()
                      .onTap(() async {
                    if (await canLaunch(
                        "https://www.youtube.com/watch?v=${widget.paths[at]}}")) {
                      launch(
                          "https://www.youtube.com/watch?v=${widget.paths[at]}}");
                    }
                  })
                ]);
              },
              itemCount: widget.paths.length,
            ),
          ).cornerRadius(15),
          Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_left,
                size: 45,
                color: Colors.white,
              ).onTap(() {
                if (page > 1) {
                  setState(() => page--);
                  controller.animateToPage(page - 1,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.ease);
                }
              }),
              "$page/${widget.paths.length}".text.white.make(),
              Icon(
                Icons.arrow_right,
                size: 45,
                color: Colors.white,
              ).onTap(() {
                if (page < widget.paths.length) {
                  setState(() => page++);
                  controller.animateToPage(page - 1,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.ease);
                }
              })
            ],
          ).box.width(Get.width).height(50).make()
        ],
      ),
    ).box.height(Get.height).width(Get.width).make();
  }
}
