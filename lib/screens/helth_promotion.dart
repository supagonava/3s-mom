import 'package:flutter/material.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:s3mom/widgets/page_template.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

class HeathPromotion extends StatefulWidget {
  const HeathPromotion({Key key}) : super(key: key);

  @override
  _HeathPromotionState createState() => _HeathPromotionState();
}

class _HeathPromotionState extends State<HeathPromotion> {
  int page1 = 0;
  int page2 = 0;
  int page3 = 0;
  int page4 = 0;
  int page5 = 0;
  int page6 = 0;
  int page7 = 0;
  List<List<String>> listImages = [
    [
      "assets/images/hp/food/advice (1).png",
      "assets/images/hp/food/advice (2).png",
      "assets/images/hp/food/advice (3).png",
      "assets/images/hp/food/advice (4).png",
      "assets/images/hp/food/exsam (1).png",
      "assets/images/hp/food/exsam (2).png",
    ],
    [
      "assets/images/hp/exec/exec1.png",
      "assets/images/hp/exec/exec2.png",
      "MoAYKz-ySv4"
    ],
    [
      "assets/images/hp/work/work1.PNG",
      "assets/images/hp/work/work2.PNG",
      "assets/images/hp/work/after_work.PNG",
    ],
    ["assets/images/hp/ubutihet/protect.png"],
    [
      "assets/images/hp/sukvittaya/IMG_6689.JPG",
      "assets/images/hp/sukvittaya/IMG_6690.JPG",
      "assets/images/hp/sukvittaya/IMG_6691.JPG",
    ],
    [
      "assets/images/hp/baby_rev/baby (1).png",
      "assets/images/hp/baby_rev/baby (2).png",
      "assets/images/hp/baby_rev/baby (3).png",
      "assets/images/hp/baby_rev/baby (4).png",
      "assets/images/hp/baby_rev/baby (5).png",
      "assets/images/hp/baby_rev/baby (6).png",
      "assets/images/hp/baby_rev/baby (7).png",
      "assets/images/hp/baby_rev/baby (8).png",
      "assets/images/hp/baby_rev/baby (9).png",
    ],
    ["assets/images/hp/covid/IMG_7935.JPG"]
  ];
  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: "การส่งเสริมสุขภาพ",
      widgets: [
        MaterialButton(
          height: 55,
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            showScrollView(context, "โภชนาการ", listImages[0]);
          },
          child: "โภชนาการ"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
        MaterialButton(
          height: 55,
          minWidth: Get.width,
          onPressed: () {
            showScrollView(context, "การออกกำลังกาย", listImages[1]);
          },
          child: "การออกกำลังกาย"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
        MaterialButton(
          height: 55,
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () => showBabyRev(context),
          child: "พัฒนาการทารกในครรภ์"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
        MaterialButton(
          height: 55,
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            showScrollView(context, "การดูแลสุขวิทยา", listImages[4]);
          },
          child: "การดูแลสุขวิทยา"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
        MaterialButton(
          height: 55,
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            showScrollView(context, "การทำงาน", listImages[2]);
          },
          child: "การทำงาน"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
        MaterialButton(
          height: 55,
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            showScrollView(context, "อุบัติเหตุ", listImages[3]);
          },
          child: "อุบัติเหตุ"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
        MaterialButton(
          height: 55,
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            showScrollView(context, "รู้เท่าทัน COCOVID-19", listImages[6]);
          },
          child: "รู้เท่าทัน COVID-19"
              .text
              .align(TextAlign.center)
              .size(16)
              .make()
              .box
              .width(Get.width * .8)
              .make(),
          color: MainColors.orange,
        ).marginOnly(bottom: 10),
      ],
    );
  }

  showBabyRev(BuildContext context) {
    Get.dialog(
      StatefulBuilder(
        builder: (context, setState) => SimpleDialog(
          title: Column(
            children: [
              "พัฒนาการทารกในครรภ์"
                  .text
                  .size(16)
                  .align(TextAlign.center)
                  .white
                  .makeCentered(),
              SizedBox(height: 10),
            ],
          ),
          backgroundColor: MainColors.pink,
          contentPadding: EdgeInsets.all(8),
          children: (() {
            return [
              GridView.count(
                crossAxisCount: 3,
                mainAxisSpacing: 5,
                crossAxisSpacing: 5,
                children: List<Widget>.generate(
                    9,
                    (index) => "เดือนที่ ${index + 1}"
                            .text
                            .bold
                            .makeCentered()
                            .box
                            .width(Get.width * .3)
                            .height(40)
                            .color(MainColors.blue)
                            .make()
                            .cornerRadius(15)
                            .onTap(() {
                          Get.back();
                          showScrollView(
                              context, "พัฒนาการทารกในครรภ์", listImages[5],
                              initPage: index);
                        })).toList(),
              ).box.width(Get.width).height(Get.height * .45).make()
            ];
          }()),
        ),
      ),
    );
  }
}
