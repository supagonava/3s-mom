import 'package:flutter/material.dart';
import 'package:s3mom/widgets/page_template.dart';

class Trimas2 extends StatefulWidget {
  const Trimas2({Key key}) : super(key: key);

  @override
  _Trimas2State createState() => _Trimas2State();
}

class _Trimas2State extends State<Trimas2> {
  @override
  Widget build(BuildContext context) {
    return PageTemplate(title: "การตั้งครรภ์ไตรมาสที่ 2", widgets: []);
  }
}
