import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
// import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/top_container.dart';
import 'package:s3mom/config.dart' as config;

class CalculatePragnant extends StatefulWidget {
  const CalculatePragnant({Key key}) : super(key: key);

  @override
  _CalculatePragnantState createState() => _CalculatePragnantState();
}

class _CalculatePragnantState extends State<CalculatePragnant> {
  int firstCasePickYear, firstCasePickDay, firstCasePickMonth = 0;
  int secondCasePickYear, secondCasePickDay, secondCasePickMonth = 0;
  int thirdCasePickYear, thirdCasePickDay, thirdCasePickMonth = 0;

  String currentPregnantAge(int day, int month, int year) {
    try {
      DateTime now = DateTime.now();
      // now = DateTime(2021, 8, 5);
      DateTime lastPeriod = DateTime(year - 543, month + 1, day);
      Duration diffDate = now.difference(lastPeriod);
      if (diffDate.inDays >= 0 && day > 0)
        return "${(diffDate.inDays ~/ 7)} สัปดาห์ ${diffDate.inDays % 7} วัน";
    } catch (e) {
      log(e.toString());
    }
    return "ข้อมูลวันที่ไม่ถูกต้องค่ะ";
  }

  String calculate37WeekPregnantAge(int day, int month, int year) {
    int addDay = 7 * 37;
    try {
      DateTime lastPeriod = DateTime(year - 543, month + 1, day);
      DateTime age = lastPeriod.add(Duration(days: addDay));
      return "${age.day} ${config.months[age.month - 1]} ${age.year + 543}";
    } catch (e) {
      log(e.toString());
    }
    return "ข้อมูลวันที่ไม่ถูกต้องค่ะ";
  }

  String calculate40WeekPregnantAge(int day, int month, int year) {
    int addDay = 7 * 40;
    try {
      DateTime lastPeriod = DateTime(year - 543, month + 1, day);
      DateTime age = lastPeriod.add(Duration(days: addDay));
      return "${age.day} ${config.months[age.month - 1]} ${age.year + 543}";
    } catch (e) {
      log(e.toString());
    }
    return "ข้อมูลวันที่ไม่ถูกต้องค่ะ";
  }

  String calculate42WeekPregnantAge(int day, int month, int year) {
    int addDay = 7 * 42;
    try {
      DateTime lastPeriod = DateTime(year - 543, month + 1, day);
      DateTime age = lastPeriod.add(Duration(days: addDay));
      return "${age.day} ${config.months[age.month - 1]} ${age.year + 543}";
    } catch (e) {
      log(e.toString());
    }
    return "ข้อมูลวันที่ไม่ถูกต้องค่ะ";
  }

  getPregnantAgeByDuedate(int day, int month, int year) {
    int removeDay = 7 * 40;
    try {
      DateTime dueDate = DateTime(year - 543, month + 1, day);
      dueDate = dueDate.subtract(Duration(days: removeDay));
      return currentPregnantAge(
          dueDate.day, dueDate.month - 1, dueDate.year + 543);
    } catch (e) {
      log(e.toString());
    }
    return "ข้อมูลวันที่ไม่ถูกต้องค่ะ";
  }

  getPregnantAgeByLMP(int year, int month, int day) {
    try {
      DateTime lmpDate = DateTime(year - 543, month + 1 - 3, day);
      lmpDate = lmpDate.add(Duration(days: 7));
      log(lmpDate.toString());
      return "${lmpDate.day} ${config.months[lmpDate.month - 1]} ${lmpDate.year + 543}";
    } catch (e) {
      log(e.toString());
    }
    return "ข้อมูลวันที่ไม่ถูกต้องค่ะ";
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                TopContainer(
                  height: 100,
                  padding: EdgeInsets.all(15),
                  width: size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 0.0),
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          runSpacing: 15,
                          children: <Widget>[
                            BackButton(
                              color: Colors.white,
                            ),
                            Container(
                              child: Text(
                                'คำนวนอายุครรภ์',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: size.height - 124,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.black12,
                              border:
                                  Border.all(color: Colors.grey, width: 1.5),
                              borderRadius: BorderRadius.circular(15)),
                          padding: const EdgeInsets.all(8.0),
                          margin: const EdgeInsets.all(8.0),
                          child: Text(
                              "วันกําหนดคลอดของคุณแม่ เป็นเพียงการคํานวณเบื้องต้นเท่านั้น\nควรได้รับการตรวจเพิ่มเติมจากคุณหมอ เพื่อความแม่นยําและเพื่อ\nสุขภาพที่ดีของลูกน้อยในครรภ์และตัวคุณแม่นะคะ"),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ExpansionTile(
                            title: Wrap(
                                alignment: WrapAlignment.start,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  Icon(Icons.calendar_today),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                      "กรณีที่ 1\nระบุวันที่มีประจำเดือนสุดท้าย")
                                ]),
                            children: [
                              Wrap(
                                direction: Axis.horizontal,
                                spacing: 8,
                                children: [
                                  Container(
                                    height: 38,
                                    margin: EdgeInsets.only(top: 7),
                                    width: size.width * .2,
                                    child: TextFormField(
                                      onChanged: (day) => setState(() =>
                                          firstCasePickDay = int.parse(day)),
                                      autocorrect: false,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: false, signed: true),
                                      decoration: InputDecoration(
                                        hintText: "วัน",
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 45,
                                    width: size.width * .3,
                                    child: DropdownButtonFormField(
                                      value: firstCasePickMonth,
                                      items: List<DropdownMenuItem>.generate(
                                        11,
                                        (index) => DropdownMenuItem(
                                          child:
                                              Text("${config.months[index]}"),
                                          value: index,
                                        ),
                                      ).toList(),
                                      onChanged: (month) => setState(
                                          () => firstCasePickMonth = month),
                                      hint: Container(
                                        margin:
                                            const EdgeInsets.only(bottom: 10),
                                        child: Text("เดือน"),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 38,
                                    margin: EdgeInsets.only(top: 7),
                                    width: size.width * .2,
                                    child: TextFormField(
                                      autocorrect: false,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: false, signed: true),
                                      decoration: InputDecoration(
                                        hintText: "พศ.",
                                      ),
                                      onChanged: (year) => setState(() =>
                                          firstCasePickYear = int.parse(year)),
                                    ),
                                  )
                                ],
                              ),
                              Divider(),
                              Text(
                                "อายุครรภ์ของคุณตอนนี้",
                                style: TextStyle(fontSize: 20),
                              ),
                              Divider(),
                              RowData(
                                  size: size,
                                  title: "ประจำเดือนล่าสุด",
                                  data:
                                      "${firstCasePickDay ?? ''} ${firstCasePickMonth != null ? config.months[firstCasePickMonth] : ''} ${firstCasePickYear ?? ''}"),
                              RowData(
                                  size: size,
                                  title: "ขณะนี้ตั้งครรภ์",
                                  data: currentPregnantAge(firstCasePickDay,
                                      firstCasePickMonth, firstCasePickYear)),
                              RowData(
                                  size: size,
                                  title: "อายุครรภ์ 37 สัปดาห์",
                                  data: calculate37WeekPregnantAge(
                                      firstCasePickDay,
                                      firstCasePickMonth,
                                      firstCasePickYear)),
                              RowData(
                                  size: size,
                                  title: "วันกำหนดคลอด",
                                  data: calculate40WeekPregnantAge(
                                      firstCasePickDay,
                                      firstCasePickMonth,
                                      firstCasePickYear)),
                              RowData(
                                  size: size,
                                  title: "อายุครรภ์ 42 สัปดาห์",
                                  data: calculate42WeekPregnantAge(
                                      firstCasePickDay,
                                      firstCasePickMonth,
                                      firstCasePickYear)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ExpansionTile(
                            title: Wrap(
                                alignment: WrapAlignment.start,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  Icon(Icons.timelapse),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text("กรณีที่ 2\nระบุวันกำหนดคลอด")
                                ]),
                            children: [
                              Wrap(
                                direction: Axis.horizontal,
                                spacing: 8,
                                children: [
                                  Container(
                                    height: 38,
                                    margin: EdgeInsets.only(top: 7),
                                    width: size.width * .2,
                                    child: TextFormField(
                                      onChanged: (day) => setState(() =>
                                          secondCasePickDay = int.parse(day)),
                                      autocorrect: false,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: false, signed: true),
                                      decoration: InputDecoration(
                                        hintText: "วัน",
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 45,
                                    width: size.width * .3,
                                    child: DropdownButtonFormField(
                                      value: secondCasePickMonth,
                                      items: List<DropdownMenuItem>.generate(
                                        11,
                                        (index) => DropdownMenuItem(
                                          child:
                                              Text("${config.months[index]}"),
                                          value: index,
                                        ),
                                      ).toList(),
                                      onChanged: (month) => setState(
                                          () => secondCasePickMonth = month),
                                      hint: Container(
                                        margin:
                                            const EdgeInsets.only(bottom: 10),
                                        child: Text("เดือน"),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 38,
                                    margin: EdgeInsets.only(top: 7),
                                    width: size.width * .2,
                                    child: TextFormField(
                                      autocorrect: false,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: false, signed: true),
                                      decoration: InputDecoration(
                                        hintText: "พศ.",
                                      ),
                                      onChanged: (year) => setState(() =>
                                          secondCasePickYear = int.parse(year)),
                                    ),
                                  )
                                ],
                              ),
                              Divider(),
                              Text(
                                "อายุครรภ์ของคุณตอนนี้",
                                style: TextStyle(fontSize: 20),
                              ),
                              Divider(),
                              RowData(
                                  size: size,
                                  title: "ขณะนี้ตั้งครรภ์",
                                  data: getPregnantAgeByDuedate(
                                      secondCasePickDay,
                                      secondCasePickMonth,
                                      secondCasePickYear)),
                              RowData(
                                  size: size,
                                  title: "วันกำหนดคลอด",
                                  data:
                                      "${secondCasePickDay ?? ''} ${secondCasePickMonth != null ? config.months[secondCasePickMonth] : ''} ${secondCasePickYear ?? ''}"),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RowData extends StatelessWidget {
  const RowData({
    Key key,
    @required this.size,
    @required this.data,
    @required this.title,
  }) : super(key: key);

  final Size size;
  final String title;
  final String data;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: size.width * 0.3,
                child: Text("$title"),
              ),
              Container(
                width: size.width * 0.6,
                child: Text("$data"),
              )
            ],
          ),
          Divider(),
        ],
      ),
    );
  }
}
