import 'dart:async';
import 'package:flutter_tts/flutter_tts.dart';

class TTS {
  static FlutterTts flutterTts = FlutterTts();

  String language;
  bool isCurrentLanguageInstalled = false;

  static Future<void> initTts() async {
    String language = "en-US";
    await flutterTts.setLanguage("$language");
    await flutterTts.setSpeechRate(1.0);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(1.0);
  }

  static Future speak(String message) async {
    await flutterTts.speak("$message");
  }

  static Future stop() async {
    await flutterTts.stop();
  }
}
