import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:s3mom/widgets/page_template.dart';
import 'package:velocity_x/velocity_x.dart';

class ScreeningPregnant extends StatefulWidget {
  const ScreeningPregnant({Key key}) : super(key: key);

  @override
  _ScreeningPregnantState createState() => _ScreeningPregnantState();
}

class _ScreeningPregnantState extends State<ScreeningPregnant> {
  final List<Map> questionList = [
    {
      'no': 1,
      'question':
          "มีเพศสัมพันธ์กับแฟนแล้วประจำเดือน(เมนส์)ไม่มาตามกำหนดมากกว่า 10 วัน",
      'info': {
        'title': 'อาการประจำเดือนขาด หรือ เมนส์ไม่มา',
        'body':
            '''	โดยทั่วไปผู้หญิงที่แต่งงานแล้วและมีร่างกายแข็งแรงสมบูรณ์ดี มีประจำเดือนมาสม่ำเสมอ เมื่อเกิดอาการขาดประจำเดือนก็สงสัยว่าน่าจะตั้งครรภ์ โดยเฉพาะถ้าประจำเดือนไม่มานานเกินกว่า กำหนด 10 วัน นั่นอาจเป็นสัญญาณที่บอกได้ว่า “กำลังตั้งครรภ์” \n
	อย่างไรก็ตามการขาดของประจำเดือนก็อาจมาจากสาเหตุอื่นที่ไม่ใช่การตั้งครรภ์ก็ได้ เช่น อยู่ในช่วงไข่ตก การใช้ยาคุมกำเนิด ร่างกายได้รับสารอาหารไม่เพียงพอ รู้สึกเครียดมากจนเกินไป หรือเป็นโรคบางอย่าง เช่น โรคของระบบต่อมไร้ท่อ เบาหวาน โรคเกี่ยวกับรังไข่      เป็นต้น 
'''
      },
      'checked': false
    },
    {
      'no': 2,
      'question': 'คัดตึงเต้านม/เต้านมขยายใหญ่ขึ้น ลานนมมีสีเข้มขึ้น',
      'info': {
        'title': 'การเปลี่ยนแปลงของเต้านม',
        'body':
            '''คุณแม่ที่กำลังตั้งครรภ์จะมีความรู้สึกถึงการเปลี่ยนแปลงของเต้านม เช่น คัดตึงเต้านม เริ่มตั้งแต่อายุครรภ์ 6 สัปดาห์ขึ้นไป เนื่องจากเกิดการกระตุ้นของฮอร์โมนชนิดหนึ่ง ทางการแพทย์เรียกว่า “ฮอร์โมนเอสโตรเจน” ทำให้เกิดการสร้างต่อมน้ำนม เต้านมขยายใหญ่ ลานนมมีสีเข้มขึ้น'''
      },
      'checked': false
    },
    {
      'no': 3,
      'question': 'ปากมดลูกหรือช่องคลอดคล้ำขึ้น หรือ ออกสีม่วงแดง',
      'info': {
        'title': 'สีของช่องคลอดเปลี่ยนแปลง',
        'body':
            '''การที่สีของช่องคลอดเข้มขึ้นเกิดจากการที่มีเลือดมาเลี้ยงมากจนคล้ำหรือออกสีม่วง และมีการหนาตัวของผิวหนังบริเวณนั้น ทางการแพทย์เรียกว่า ‘แชดวิค ไซน์’ (Chadwick’s sign) เพื่อเตรียมพร้อมสำหรับการคลอด นอกจากนี้ยังทำให้มีตกขาวมากขึ้น'''
      },
      'checked': false
    },
    {
      'no': 4,
      'question': 'เส้นแนวตั้งสีน้ำตาลหรือสีดำที่เกิดขึ้นกลางท้อง',
      'info': {
        'title': 'การเปลี่ยนแปลงของผิวหนัง',
        'body':
            '''มีสาเหตุเช่นเดียวกับการเปลี่ยนแปลงของเต้านมนั้นก็คือเป็นผลมาจากฮอร์โมนที่ชื่อว่าเอสโตรเจน และยังมีอีกหนึ่งฮอร์โมนเพิ่มเข้ามา ในทางการแพทย์เรียกว่า ‘ฮอร์โมนโปรเจสเตอโรน’ มีผลเพิ่มความเข้มของผิวหนังหรือสร้างเม็ดสีที่เพิ่มขึ้น จนเกิดเป็นรอยเส้นสีดำที่กลางท้องของคุณแม่ อีกทั้งยังมีผลต่อการเปลี่ยนแปลงของผิวหนังส่วนอื่นในระหว่างการตั้งครรภ์ด้วย เช่น บริเวณหัวนม ไฝ ปาน และกระ จะมีสีเข้มขึ้นด้วยเช่นกัน\nอีกสาเหตุก็คือการยืดขยายของกล้ามเนื้อบริเวณหน้าท้อง เรียกว่าหน้าท้องของเราจะยิ่งขยายไปตามอายุครรภ์ที่มากขึ้นนั่นเอง'''
      },
      'checked': false
    },
    {
      'no': 5,
      'question': 'คลื่นไส้ อาเจียนหรือแพ้ท้อง',
      'info': {
        'title': 'อาการคลื่นไส้ (อาจมีอาเจียนร่วมด้วย)',
        'body':
            '''ในระหว่างที่คุณแม่กำลังตั้งครรภ์จะเกิดการเปลี่ยนแปลงของฮอร์โมนทำให้รบกวนการทำงานของระบบทางเดินอาหารได้บ่อยๆ เป็นผลให้มีอาการคลื่นไส้อาเจียน นอกจากนี้ยังทำให้เกิดอาการท้องผูก ท้องอืด รู้สึกไม่สบายท้องได้อีกด้วย\nอีกสาเหตุหนึ่งที่ทำให้เกิดอาการแพ้ท้องก็คืออาการที่จมูกจะไวต่อกลิ่นทุกชนิดมากเป็นพิเศษ บางรายมีอาการเหม็นกลิ่นอาหาร กลิ่นตัวสามี เป็นต้น ซึ่งเป็นกลไกตามธรรมชาติที่จะพยายามป้องกันร่างกายของคุณแม่ที่กำลังตั้งครรภ์'''
      },
      'checked': false
    },
    {
      'no': 6,
      'question': 'มีความรู้สึกว่าปัสสาวะบ่อยขึ้น',
      'info': {
        'title': 'ปัสสาวะบ่อย',
        'body':
            '''ในช่วงไตรมาสแรกของการตั้งครรภ์ หรือประมาณ 3 เดือนแรก คุณแม่จะรู้สึกอยากปัสสาวะบ่อย เชื่อว่าเกิดจากการที่มดลูกมีขนาดโตขึ้นจนไปเบียดกับกระเพาะปัสสาวะ\nแต่ในคนทั่วไปที่มีการติดเชื้อของทางเดินปัสสาวะ เนื้องอกในเชิงกราน เป็นโรคเบาหวาน หรือมีภาวะเครียดก็เป็นสาเหตุของการปัสสาวะบ่อยได้ '''
      },
      'checked': false
    },
    {
      'no': 7,
      'question': 'เหนื่อย อ่อนเพลีย และง่วงนอน',
      'info': {
        'title': 'อาการอ่อนเพลีย',
        'body':
            '''คุณแม่ที่ตั้งครรภ์ในช่วงไตรมาสแรกหรือประมาณ 3 เดือนแรกจะรู้สึกอ่อนเพลียและง่วงนอน เป็นเพราะในระหว่างตั้งครรภ์ร่างกายจะใช้พลังงานมากกว่าปกติ อีกทั้งทารกที่กำลังเจริญเติบโตในครรภ์มีความต้องการออกซิเจนจากคุณแม่จึงส่งผลให้คุณแม่มีอาการหายใจถี่ เหนื่อย และอ่อนเพลียได้'''
      },
      'checked': false
    },
    {
      'no': 8,
      'question': 'ท้องโตขึ้น',
      'info': {
        'title': 'ท้องโตขึ้น',
        'body':
            '''มีสาเหตุมาจากทารกน้อยที่กำลังเติบโตอยู่ในครรภ์ โดยสามารถตรวจพบได้ตั้งแต่อายุครรภ์ 7 สัปดาห์ขึ้นไป และเมื่ออายุครรภ์ได้ 10 สัปดาห์จะสามารถคลำพบก้อนมดลูกบริเวณเหนือท้องน้อยได้ชัดเจน และจะโตขึ้นเรื่อยๆ เมื่ออายุครรภ์ของคุณแม่เพิ่มขึ้น\nอย่างไรก็ตามอาการท้องโตก็สามารถเกิดขึ้นได้เนื่องจากสาเหตุอื่น เช่น โรคอ้วน เนื้องอกในช่องท้องหรือเชิงกราน ท้องมาน เป็นต้น'''
      },
      'checked': false
    },
    {
      'no': 9,
      'question': 'รู้สึกตึงๆที่ท้องน้อยเป็นครั้งคราวแต่ว่าไม่รู้สึกปวด',
      'info': {
        'title': 'การซ้อมการหดรัดตัวของมดลูก',
        'body':
            '''โดยทั่วไปแล้วคุณแม่ที่กำลังตั้งครรภ์จะมีการหดรัดตัวของมดลูกเป็นครั้งคราว เป็นการซ้อมการหดรัดตัวของมดลูกเพื่อการคลอด พบได้ที่อายุครรภ์ตั้งแต่ 12 สัปดาห์เป็นต้นไป และการหดรัดตัวของมดลูกจะเพิ่มมากขึ้นตามอายุครรภ์ \nคุณแม่จะรู้สึกตึงๆที่ส่วนบนของท้องน้อยแต่จะไม่ปวด ปากมดลูกจะไม่เปิด หากคุณแม่ได้พักอาการจะดีขึ้นและหายไปเอง แต่ในทางกลับกันถ้าไปกระตุ้นด้วยการนวดคลึงบริเวณท้องน้อยจะทำให้ทีการหดรัดตัวถี่และรุนแรงขึ้น'''
      },
      'checked': false
    },
    {
      'no': 10,
      'question': 'คลำที่หน้าท้องแล้วรู้สึกว่าพบขอบเขตของทารก',
      'info': {
        'title': 'คลำได้ขอบเขตของทารก',
        'body':
            '''เป็นเพราะทารกที่กำลังเจริญเติบโตอยู่ในครรภ์จึงทำให้มดลูกขยายใหญ่ ตามปกติแล้วขอบเขตของทารกน้อยสามารถคลำได้ตั้งแต่อายุครรภ์ 22 สัปดาห์ขึ้นไป โดยเฉพาะในคุณแม่ที่มีรูปร่างผอมผนังท้องจะไม่หนา ยิ่งอายุครรภ์มากก็จะยิ่งคลำได้ชัดเจนขึ้น'''
      },
      'checked': false
    },
    {
      'no': 11,
      'question': 'ใช้มือสัมผัสที่หน้าท้องแล้วพบว่าลูกดิ้น',
      'info': {
        'title': 'ตรวจพบการเคลื่อนไหวของทารกในครรภ์',
        'body':
            '''	เมื่อคุณแม่ตั้งครรภ์ได้ประมาณ 5 เดือน หรือ 20 สัปดาห์ กระดูกและกล้ามเนื้อของทารกน้อยก็จะเจริญเติบโต เริ่มเหยียดแขนยืดขาเคลื่อนไหวไปมาอยู่ในน้ำคร่ำ เวลาที่แขนขาของทารกไปกระทบกับผนังมดลูกของคุณแม่ ก็จะทำให้รู้สึกได้ถึงการเคลื่อนไหวของทารกในครรภ์ การเคลื่อนไหวของทารกในครรภ์ไม่เพียงแต่ทำให้รู้สึกว่ามีเด็กอยู่ในท้องเท่านั้น แต่ยังเป็นการสื่อสารที่สำคัญที่ทารกบอกให้คุณแม่รู้ถึงสภาพร่างกายของตัวเองอีกด้วย'''
      },
      'checked': false
    },
  ];
  double getResult() {
    double result = 0;
    try {
      bool isSelected1to7 = false;
      bool isSelected1in7 = false;
      bool isSelected1in10 = false;
      for (var map in questionList) {
        if (map['no'] == 1 && map['checked']) {
          isSelected1to7 = true;
          isSelected1in7 = true;
          isSelected1in10 = true;
        } else {
          if (isSelected1to7 && map['checked'] && map['no'] <= 7) {
            isSelected1to7 = true;
          } else {
            isSelected1to7 = false;
          }
          if (map['no'] <= 10 && map['checked']) {
            if (map['no'] <= 7) {
              isSelected1in7 = true;
            } else {
              isSelected1in10 = true;
            }
          }
        }
      }

      if (isSelected1to7) {
        result = 50;
      }
      if (isSelected1in7 &&
          (questionList[6]['checked'] ||
              questionList[7]['checked'] ||
              questionList[8]['checked'])) {
        result = 70;
      }
      if (isSelected1in10 && questionList[10]['checked']) {
        result = 100;
      }

      if (result == 0) {
        int checkedList =
            questionList.where((element) => element['checked']).toList().length;
        result = checkedList * 50 / 7;
      }
    } catch (e) {
      log("$e");
    }
    log("$result");
    return result / 100;
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return PageTemplate(
      title: "เกณ์ประเมินความเสี่ยงของหญิงตั้งครรภ์เมื่อมาฝากครรภ์",
      widgets: [
        Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              border: Border.all(color: Colors.black12)),
          width: size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "คำชี้แจง",
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.start,
              ),
              Text(
                "ข้อความต่อไปนี้เป็นอาการที่สงสัยว่าคุณจะตั้งครรภ์\nขอให้คุณสังเกตตัวเอง\n\nหากมีอาการดังกล่าว\nให้ทำเครื่องหมาย ✓ ในข้อนั้น\n\nท่านสามารถแตะค้างเพื่ออ่านรายละเอียดในแต่ละข้อได้",
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
        Column(
          children: List<StatelessWidget>.generate(
            questionList.length,
            (index) => Card(
              color: questionList[index]['checked']
                  ? Colors.blue[50]
                  : Colors.white,
              child: GestureDetector(
                onLongPress: () => showAlertDialog(
                    "${questionList[index]['info']['title']}",
                    ["${questionList[index]['info']['body']}".text.make()]),
                child: CheckboxListTile(
                  activeColor: MainColors.blue,
                  onChanged: (value) =>
                      setState(() => questionList[index]['checked'] = value),
                  value: questionList[index]['checked'],
                  title:
                      Text("${index + 1}. ${questionList[index]['question']}"),
                ),
              ),
            ),
          ).toList(),
        ),
        MaterialButton(
          minWidth: size.width,
          color: MainColors.blue,
          child: Text("ดูผลลัพธ์",
              style: TextStyle(fontSize: 16, color: Colors.white)),
          onPressed: () => showAlertDialog("ผลการประเมิน", [
            Center(
              child: CircularPercentIndicator(
                radius: 150,
                lineWidth: 10,
                percent: getResult(),
                progressColor: Colors.blue,
                center: Text(
                  "${(getResult() * 100).toStringAsFixed(2)} %",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w400),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Center(
              child: Text(
                "คุณมีโอกาสตั้งครรภ์ ${(getResult() * 100).toStringAsFixed(2)} %",
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Text(
                "หากคุณมีอาการข้างต้น สงสัยว่าคุณอาจจจะตั้งครรภ์ ควรปรึกษาเจ้าหน้าที่สาธารณสุขใกล้บ้าน หรือ ตรวจสอบการตั้งครรภ์ด้วยชุดตรวจการตั้งครรภ์ หรือ สอบถามมายังไลน์ต่อไปนี้ (แอดไลน์ official line)",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 10),
              ),
            ),
          ]),
        ),
      ],
    );
  }
}
