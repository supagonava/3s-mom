import 'package:flutter/material.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/page_template.dart';
import 'package:velocity_x/velocity_x.dart';

class ImportalOfAnnetal extends StatefulWidget {
  const ImportalOfAnnetal({Key key}) : super(key: key);

  @override
  _ImportalOfAnnetalState createState() => _ImportalOfAnnetalState();
}

class _ImportalOfAnnetalState extends State<ImportalOfAnnetal> {
  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: "ความสำคัญของการฝากครรภ์",
      widgets: [
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  showAlertDialog("ความสำคัญของการฝากครรภ์", []);
                },
                child: "ความสำคัญของการฝากครรภ์".text.size(16).white.make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  showAlertDialog(
                      "ความสำคัญของการฝากครรภ์ก่อนอายุครรภ์ 12 สัปดาห์", []);
                },
                child: "ความสำคัญของการฝากครรภ์ก่อนอายุครรภ์ 12 สัปดาห์"
                    .text
                    .align(TextAlign.center)
                    .size(16)
                    .white
                    .make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  showAlertDialog("โรคธาลัสซีเมีย", []);
                },
                child: "โรคธาลัสซีเมีย".text.size(16).white.make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  showAlertDialog("ภาวะดาวน์ซินโดรม", []);
                },
                child: "ภาวะดาวน์ซินโดรม".text.size(16).white.make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
        MaterialButton(
                minWidth: double.infinity,
                height: 60,
                color: MainColors.blue,
                onPressed: () {
                  showAlertDialog("พัฒนาการทารกในครรภ์", []);
                },
                child: "พัฒนาการทารกในครรภ์".text.size(16).white.make())
            .box
            .margin(EdgeInsets.only(top: 8))
            .make(),
      ],
    );
  }
}
