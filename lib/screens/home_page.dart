import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:s3mom/config.dart' as config;
import 'package:s3mom/main.dart';
import 'package:s3mom/router.dart' as router;
import 'package:s3mom/screens/health_care.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:s3mom/widgets/home_button.dart';
import 'package:s3mom/widgets/image_view.dart';
import 'package:s3mom/widgets/top_container.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () async {
          final result = await Get.defaultDialog(
            title: "ต้องการออกจากแอพ?",
            content: RichText(
                text: TextSpan(children: [
              blackTextSpan(
                  "ทางทีมพัฒนาขอรบกวนคุณแม่ในการประเมินความพึงพอใจต่อแอพลิเคชั่นโดยการ"),
              "คลิกที่นี่ค่ะ".textSpan.underline.blue500.make()
            ])).box.width(Get.width * .75).make().onTap(() async {
              if (await canLaunch("https://forms.gle/fXiYnMugzwva9EiA9")) {
                launch("https://forms.gle/fXiYnMugzwva9EiA9");
              }
            }),
            onConfirm: () {
              Get.back(result: true);
            },
            textConfirm: "ใช่",
            confirmTextColor: Colors.white,
            textCancel: "ไม่",
          );
          return result != null;
        },
        child: SafeArea(
          child: Column(
            children: <Widget>[
              TopContainer(
                padding: EdgeInsets.all(8),
                height: 100,
                width: size.width,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 0.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor: MainColors.pink,
                              radius: 35.0,
                              backgroundImage: AssetImage(
                                'assets/images/logo.jpeg',
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    'หน้าหลัก',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 22.0,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    '${config.appName}',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ]),
              ),
              "คู่มือการดูแลครรภ์คุณแม่มือใหม่"
                  .text
                  .size(16)
                  .make()
                  .pOnly(top: 8),
              Container(
                height: size.height - 160,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Positioned(
                            top: 45,
                            left: size.width * 0.50,
                            child: Container(
                              color: MainColors.yellow,
                              width: size.width * .34,
                              height: 10,
                            ),
                          ),
                          Positioned(
                            top: 45,
                            right: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              width: 10,
                              height: size.height,
                            ),
                          ),
                          Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: Image.asset(
                                    "assets/images/mother.png",
                                    // width: size.width,
                                    height: 110,
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    // fontSize: 11,
                                    icon: FontAwesomeIcons.check,
                                    label: "การประเมินการตั้งครรภ์",
                                    onClick: () => Navigator.of(context)
                                        .pushNamed(router.checkListPage),
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: FontAwesomeIcons.listOl,
                                    label: "เตรียมตัวไปฝากครรภ์",
                                    onClick: () => Navigator.of(context)
                                        .pushNamed(router.stepToAnnetal),
                                  ),
                                ),
                              ),
                            ],
                          ).box.height(Get.height * .18).make()
                        ],
                      ),
                      Stack(
                        children: [
                          Positioned(
                            top: 45,
                            left: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              height: 10,
                              width: size.width * .68,
                            ),
                          ),
                          Positioned(
                            top: 0,
                            right: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              width: 10,
                              height: 45,
                            ),
                          ),
                          Positioned(
                            top: 45,
                            left: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              width: 10,
                              height: size.height,
                            ),
                          ),
                          Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: Icons.healing,
                                    label: "การส่งเสริมสุขภาพ",
                                    onClick: () {
                                      Navigator.of(context)
                                          .pushNamed(router.healthPromotion);
                                    },
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: Icons.local_hospital,
                                    label: "การดูแลตัวเอง",
                                    onClick: () =>
                                        Get.to(() => HealthCarePage()),
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: Icons.sick,
                                    label: "ภาวะแทรกซ้อน",
                                    onClick: () => showScrollView(
                                        context,
                                        "ภาวะแทรกซ้อน",
                                        ["assets/images/complication.png"]),
                                  ),
                                ),
                              ),
                            ],
                          ).box.height(Get.height * .18).make(),
                        ],
                      ),
                      Stack(
                        children: [
                          Positioned(
                            top: 45,
                            left: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              height: 10,
                              width: size.width * .68,
                            ),
                          ),
                          Positioned(
                            top: 0,
                            left: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              height: 45,
                              width: 10,
                            ),
                          ),
                          Positioned(
                            top: 45,
                            right: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              height: size.height,
                              width: 10,
                            ),
                          ),
                          Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: FontAwesomeIcons.locationArrow,
                                    label: "สถานที่ฝากครรภ์ใกล้เคียง",
                                    onClick: () => showScrollView(
                                        context, "สถานที่ฝากครรภ์ใกล้เคียง", [
                                      "assets/images/สถานที่ฝากครรภ์ใกล้เคียง.png"
                                    ]),
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: Icons.message,
                                    label: "แชทกับเรา",
                                    onClick: () =>
                                        showAlertDialog("แชทกับเรา", [
                                      ImageView(
                                        height: Get.height * .35,
                                        isAsset: true,
                                        imagePath: "assets/images/line qr.jpg",
                                      ),
                                      "เปิดบน Line"
                                          .text
                                          .white
                                          .makeCentered()
                                          .box
                                          .width(Get.width * .95)
                                          .height(30)
                                          .green400
                                          .make()
                                          .card
                                          .make()
                                          .onTap(() async {
                                        if (await canLaunch(
                                            "https://lin.ee/CMqZ5Cb")) {
                                          launch("https://lin.ee/CMqZ5Cb");
                                        }
                                      })
                                    ]),
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: Icons.book,
                                    label: "คู่มือการใช้งานแอพลิเคชั่น",
                                    onClick: () => showAlertDialog(
                                        "คู่มือการใช้งานแอพลิเคชั่น", [
                                      ImageView(
                                        height: Get.height * .35,
                                        isAsset: true,
                                        imagePath: "assets/images/คู่มือ.png",
                                      ),
                                      "เปิดคู่มือ"
                                          .text
                                          .white
                                          .makeCentered()
                                          .box
                                          .width(Get.width * .95)
                                          .height(30)
                                          .green400
                                          .make()
                                          .card
                                          .make()
                                          .onTap(() async {
                                        if (await canLaunch(
                                            "https://anyflip.com/pvukm/fvvv/")) {
                                          launch(
                                              "https://anyflip.com/pvukm/fvvv/");
                                        }
                                      })
                                    ]),
                                  ),
                                ),
                              ),
                            ],
                          ).box.height(Get.height * .18).make(),
                        ],
                      ),
                      Stack(
                        children: [
                          Positioned(
                            top: 0,
                            right: size.width * 0.16,
                            child: Container(
                              color: MainColors.yellow,
                              width: 10,
                              height: 45,
                            ),
                          ),
                          Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  width: size.width,
                                  child: HomeButton(
                                    size: size,
                                    icon: Icons.developer_board,
                                    label: "ทีมพัฒนา",
                                    onClick: () => showScrollView(
                                        context,
                                        "ทีมพัฒนา",
                                        ["assets/images/ทีมพัฒนา.png"]),
                                  ),
                                ),
                              ),
                            ],
                          ).box.height(Get.height * .18).make(),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ).box.height(Get.height).make(),
        ),
      ),
    );
  }
}
