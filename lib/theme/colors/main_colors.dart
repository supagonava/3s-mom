import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainColors {
  static final pink = Color(0xfff99b9b);
  static final orange = Color(0xfff8bc9b);
  static final yellow = Color(0xfff8d49b);
  static final blue = Color(0xff75bde0);
  static final darkBlue = Colors.blueGrey[800];
}
