import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:s3mom/theme/colors/main_colors.dart';

class HomeButton extends StatelessWidget {
  final IconData icon;
  final String label;
  final Function onClick;
  final Size size;
  final double fontSize;
  const HomeButton({
    Key key,
    this.icon,
    this.label,
    this.onClick,
    this.size,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: MainColors.blue,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: size.width * 0.25,
          height: 108,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  child: Icon(
                    icon,
                    color: Colors.white,
                  ),
                  backgroundColor: MainColors.blue,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  color: Colors.white,
                  child: Text(
                    "$label",
                    style: TextStyle(color: MainColors.darkBlue, fontSize: 13),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: onClick,
    );
  }
}
