import 'package:flutter/material.dart';
import 'package:s3mom/widgets/top_container.dart';

class PageTemplate extends StatefulWidget {
  final List<Widget> widgets;
  final String title;
  const PageTemplate({Key key, @required this.widgets, @required this.title})
      : super(key: key);

  @override
  _PageTemplateState createState() => _PageTemplateState();
}

class _PageTemplateState extends State<PageTemplate> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TopContainer(
                height: 100,
                padding: EdgeInsets.all(15),
                width: size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 0, vertical: 0.0),
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        runSpacing: 15,
                        children: <Widget>[
                          BackButton(
                            color: Colors.white,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .65,
                            child: Text(
                              widget.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: size.height - 124,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(children: widget.widgets),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
