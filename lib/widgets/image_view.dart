import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:s3mom/theme/colors/main_colors.dart';
import 'package:velocity_x/velocity_x.dart';

class ImageView extends StatelessWidget {
  final String imagePath;
  final bool isAsset;
  final double width, height;
  const ImageView(
      {Key key, this.imagePath, this.isAsset, this.width, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ImagePreview(
                    isAsset: isAsset,
                    imagePath: imagePath,
                  ))),
      child: Stack(
        children: [
          // Positioned(child: Icon(Icons.zoom_in)),
          Container(
            width: width,
            height: height,
            child: Hero(
              child: isAsset == null || isAsset
                  ? Image.asset(
                      imagePath,
                      alignment: Alignment.center,
                    )
                  : Image.network(
                      imagePath,
                      alignment: Alignment.center,
                    ),
              tag: '$imagePath',
            ),
          ),
        ],
      ),
    );
  }
}

class ImagePreview extends StatelessWidget {
  final String imagePath;
  final bool isAsset;
  final bool withBackButton;
  const ImagePreview({
    Key key,
    @required this.imagePath,
    this.isAsset,
    this.withBackButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Hero(
          tag: '$imagePath',
          child: PhotoView.customChild(
            maxScale: 5.0,
            minScale: 1.0,
            child: ClipRect(
              child: isAsset == null || isAsset
                  ? Image.asset(
                      "$imagePath",
                      fit: BoxFit.contain,
                    )
                  : Image.network(
                      "$imagePath",
                      fit: BoxFit.contain,
                    ),
            ).box.white.make(),
          )),
      floatingActionButton: withBackButton == null
          ? MaterialButton(
              onPressed: () => Get.back(),
              shape: CircleBorder(),
              color: MainColors.blue,
              height: 60,
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 30,
              ).marginOnly(left: 10),
            )
          : Container(),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterFloat,
    ));
  }
}
