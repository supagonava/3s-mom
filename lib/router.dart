import 'package:flutter/material.dart';
import 'package:s3mom/main.dart';
import 'package:s3mom/screens/calculate_pragnant.dart';
import 'package:s3mom/screens/check_list_pregnant.dart';
import 'package:s3mom/screens/complication.dart';
import 'package:s3mom/screens/helth_promotion.dart';
import 'package:s3mom/screens/home_page.dart';
import 'package:s3mom/screens/importal_of_annetal.dart';
import 'package:s3mom/screens/screening_pregnant.dart';
import 'package:s3mom/screens/step_to_annetal.dart';
import 'package:s3mom/screens/trimas_1.dart';
import 'package:s3mom/screens/trimas_2.dart';

const String homePage = "/";
const String pregnantCalculatePage = "/pregnantCalculte";
const String checkListPage = '/checkListPregnant';
const String importalOfAntenetal = "/theImportanceOfAntenatalCare";
const String stepToAnnetal = "/stepToAnnetal";
const String screening = "/screening";
const String trimas1 = "/trimas1";
const String trimas2 = "/trimas2";
const String complication = "/complication";
const String healthPromotion = "/healthPromotion";
const String placeToAntenetal = "/placeToAntenetal";
const String chatWithUs = "/chatWithUs";

Route generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case homePage:
      return MaterialPageRoute(builder: (context) => HomePage());
      break;
    case pregnantCalculatePage:
      return MaterialPageRoute(builder: (builder) => CalculatePragnant());
      break;
    case checkListPage:
      return MaterialPageRoute(builder: (builder) => CheckListPregnant());
      break;
    case importalOfAntenetal:
      return MaterialPageRoute(builder: (builder) => ImportalOfAnnetal());
      break;
    case stepToAnnetal:
      return MaterialPageRoute(builder: (builder) => StepToAnnetal());
      break;
    case screening:
      return MaterialPageRoute(builder: (builder) => ScreeningPregnant());
    case trimas1:
      return MaterialPageRoute(builder: (builder) => Trimas1());
    case trimas2:
      return MaterialPageRoute(builder: (builder) => Trimas2());
      break;

    case healthPromotion:
      return MaterialPageRoute(builder: (builder) => HeathPromotion());
      break;
    default:
      return MaterialPageRoute(
        builder: (context) => UndefinedView(
          name: settings.name,
        ),
      );
  }
}
